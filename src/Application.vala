namespace Ootachi {
	public class Application : Gtk.Application {
		private Widgets.MainWindow window;

		construct {
			this.application_id = "org.neocities.tromino.OotachiStudio";

			this.activate.connect (() => {
				this.window = new Widgets.MainWindow (this);

				var css_provider = new Gtk.CssProvider ();
				css_provider.load_from_resource ("/org/neocities/tromino/OotachiStudio/stylesheet.css");

				Gtk.StyleContext.add_provider_for_display (
					this.window.get_display (),
					css_provider,
					Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
				);

				this.window.present ();
			});
		}

		public static int main (string[] args) {
			var app = new Application ();

			return app.run (args);
		}
	}
}
