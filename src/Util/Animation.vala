namespace Ootachi.Util {
	public class Animation : Object {
		public enum TweenType {
			LINEAR,
			SINE
		}

		private double last_target;
		private DateTime time_started = new DateTime.now_utc ();

		public TimeSpan duration { get; set; }
		public TweenType tween_type { get; set; }

		private double _target;

		public double target {
			get {
				return this._target;
			}

			set construct {
				this.last_target = this.value;
				this.time_started = new DateTime.now_utc ();
				this._target = value;
			}
		}

		public bool playing {
			get {
				var time_since_start = new DateTime.now_utc ()
					.difference (this.time_started);

				var progress = Math.fmin (
					((double) time_since_start) / ((double) this.duration),
					1
				);

				return progress < 1;
			}
		}

		public double value {
			get {
				var time_since_start = new DateTime.now_utc ()
					.difference (this.time_started);

				var progress = Math.fmin (
					((double) time_since_start) / ((double) this.duration),
					1
				);

				double progress_eased;

				switch (this.tween_type) {
					case LINEAR:
						progress_eased = progress;
						break;

					default:
					case SINE:
						progress_eased = Math.sin (progress * Math.PI_2);
						break;
				}

				return this.last_target + (
					(this.target - this.last_target) * progress_eased
				);
			}
		}

		public Animation (
			double initial_value,
			TimeSpan duration,
			TweenType tween_type
		) {
			Object (
				duration: duration,
				tween_type: tween_type,
				target: initial_value
			);
		}

		construct {
			this.last_target = this.target;
		}
	}
}
