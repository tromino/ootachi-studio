namespace Ootachi.Util {
	public interface WatchableMap<K, V> : Object, Gee.Map<K, V> {
		public abstract signal void items_changed ();
	}
}
