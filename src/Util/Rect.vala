namespace Ootachi.Util {
	public struct Rect {
		int x;
		int y;
		uint width;
		uint height;
	}
}
