namespace Ootachi.Util {
	public class WatchableHashMap<K, V> : Gee.HashMap<K, V>, WatchableMap<K, V> {
		private bool changing_all = false;

		public override void @set (K key, V value) {
			base.set (key, value);

			if (!this.changing_all) {
				this.items_changed ();
			}
		}

		public override bool unset (K key, out V value = null) {
			var changed = base.unset (key, out value);

			if (!this.changing_all && changed) {
				this.items_changed ();
			}

			return changed;
		}

		public void set_all (Gee.Map<K, V> map) {
			this.changing_all = true;
			base.set_all (map);
			this.changing_all = false;
			this.items_changed ();
		}

		public bool unset_all (Gee.Map<K, V> map) {
			this.changing_all = true;
			var changed = base.unset_all (map);
			this.changing_all = false;

			if (changed) {
				this.items_changed ();
			}

			return changed;
		}

		public override void clear () {
			base.clear ();

			// Object seemingly can sometimes be finalized by this point, so make sure it hasn't...
			if (this.ref_count > 0) {
				this.items_changed ();
			}
		}
	}
}
