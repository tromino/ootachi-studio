namespace Ootachi.Util {
	public class GridDescriptor : Object {
		// Quant sizes include single 1px border on each axis
		private double _hquant_size;
		private double _vquant_size;

		private uint _hquants_in_cell;
		private uint _vquants_in_cell;
		private uint _cells_in_beat;
		private uint _beats_in_bar;

		private Gtk.Orientation _orientation;
		private bool _display_sharps;

		public double hquant_size {
			get {
				return this._hquant_size;
			}

			set {
				if (value < 0.01) {
					warning ("GridDescriptor hquant-size cannot be less than 0.01");
				}

				this._hquant_size = Math.fmax (value, 0.01);
				this.grid_changed ();
			}
		}

		public double vquant_size {
			get {
				return this._vquant_size;
			}

			set {
				if (value < 0.01) {
					warning ("GridDescriptor vquant-size cannot be less than 0.01");
				}

				this._vquant_size = Math.fmax (value, 0.01);
				this.grid_changed ();
			}
		}

		public uint hquants_in_cell {
			get {
				return this._hquants_in_cell;
			}

			set {
				if (value < 1) {
					warning ("GridDescriptor hquants-in-cell cannot be less than 1");
				}

				this._hquants_in_cell = uint.max (value, 1);
				this.grid_changed ();
			}
		}

		public uint vquants_in_cell {
			get {
				return this._vquants_in_cell;
			}

			set {
				if (value < 1) {
					warning ("GridDescriptor vquants-in-cell cannot be less than 1");
				}

				this._vquants_in_cell = uint.max (value, 1);
				this.grid_changed ();
			}
		}

		public uint cells_in_beat {
			get {
				return this._cells_in_beat;
			}

			set {
				if (value < 1) {
					warning ("GridDescriptor cells-in-beat cannot be less than 1");
				}

				this._cells_in_beat = uint.max (value, 1);
				this.grid_changed ();
			}
		}

		public uint beats_in_bar {
			get {
				return this._beats_in_bar;
			}

			set {
				if (value < 1) {
					warning ("GridDescriptor beats-in-bar cannot be less than 1");
				}

				this._beats_in_bar = uint.max (value, 1);
				this.grid_changed ();
			}
		}

		public Gtk.Orientation orientation {
			get {
				return this._orientation;
			}

			set {
				this._orientation = value;
				this.grid_changed ();
			}
		}

		public bool display_sharps {
			get {
				return this._display_sharps;
			}

			set {
				this._display_sharps = value;
				this.grid_changed ();
			}
		}

		public GridDescriptor (
			double hquant_size,
			double vquant_size,
			uint hquants_in_cell,
			uint vquants_in_cell,
			uint cells_in_beat,
			uint beats_in_bar,
			Gtk.Orientation orientation,
			bool display_sharps
		) {
			Object (
				hquant_size: hquant_size,
				vquant_size: vquant_size,
				hquants_in_cell: hquants_in_cell,
				vquants_in_cell: vquants_in_cell,
				cells_in_beat: cells_in_beat,
				beats_in_bar: beats_in_bar,
				orientation: orientation,
				display_sharps: display_sharps
			);
		}

		// Get the area in pixels that a quant-space Rect would cover
		public Rect get_pixel_area (Rect input) {
			var in_from_x = input.x;
			var in_from_y = input.y;
			var in_to_x = input.x + input.width;
			var in_to_y = input.y + input.height;

			var out_from_x = (int) Math.round (in_from_x * this.hquant_size);
			var out_from_y = (int) Math.round (in_from_y * this.vquant_size);
			var out_to_x = int.max (((int) Math.round (in_to_x * this.hquant_size)) - 1, out_from_x);
			var out_to_y = int.max (((int) Math.round (in_to_y * this.vquant_size)) - 1, out_from_y);

			return Rect () {
				x = out_from_x,
				y = out_from_y,
				width = out_to_x - out_from_x,
				height = out_to_y - out_from_y
			};
		}

		// Get the area in quants that a pixel-space Rect would intersect (expanding outward)
		public Rect get_intersecting (Rect input) {
			var in_from_x = input.x;
			var in_from_y = input.y;
			var in_to_x = input.x + input.width;
			var in_to_y = input.y + input.height;

			var out_from_x = (int) Math.floor (in_from_x / this.hquant_size);
			var out_from_y = (int) Math.floor (in_from_y / this.vquant_size);
			var out_to_x = (int) Math.ceil ((in_to_x + 1) / this.hquant_size);
			var out_to_y = (int) Math.ceil ((in_to_y + 1) / this.vquant_size);

			return Rect () {
				x = out_from_x,
				y = out_from_y,
				width = out_to_x - out_from_x,
				height = out_to_y - out_from_y
			};
		}

		public signal void grid_changed ();
	}
}
