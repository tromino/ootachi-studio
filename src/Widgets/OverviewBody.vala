namespace Ootachi.Widgets {
	public class OverviewBody : Gtk.Widget, Gtk.Scrollable {
		private GridBackdrop backdrop;

		public uint bar_count { get; set; default = 64; }
		public uint channel_count { get; set; default = 4; }

		public uint cell_width {
			get { return this.backdrop.cell_width; }
			set { this.backdrop.cell_width = value; }
		}

		public uint cell_height {
			get { return this.backdrop.cell_height; }
			set { this.backdrop.cell_height = value; }
		}

		public uint cells_in_beat {
			get { return this.backdrop.cells_in_beat; }
			set { this.backdrop.cells_in_beat = value; }
		}

		public uint beats_in_bar {
			get { return this.backdrop.beats_in_bar; }
			set { this.backdrop.beats_in_bar = value; }
		}

		public Gtk.Orientation grid_orientation {
			get { return this.backdrop.grid_orientation; }
			set { this.backdrop.grid_orientation = value; }
		}

		private Gtk.Adjustment _hadjustment;
		private Gtk.Adjustment _vadjustment;

		public Gtk.Adjustment hadjustment {
			get { return this._hadjustment; }

			set construct {
				if ((Gtk.Adjustment?) value != null) {
					value.upper = this.measure_backdrop_width ();

					value.value_changed.connect (() => {
						this.queue_allocate ();
					});

					value.notify["upper"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});

					value.notify["page-size"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});
				}

				this._hadjustment = value;
			}
		}

		public Gtk.Adjustment vadjustment {
			get { return this._vadjustment; }

			set construct {
				if ((Gtk.Adjustment?) value != null) {
					value.upper = this.measure_backdrop_height ();

					value.value_changed.connect (() => {
						this.queue_allocate ();
					});

					value.notify["upper"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});

					value.notify["page-size"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});
				}

				this._vadjustment = value;
			}
		}

		public Gtk.ScrollablePolicy hscroll_policy { get; set; default = NATURAL; }
		public Gtk.ScrollablePolicy vscroll_policy { get; set; default = NATURAL; }

		class construct {
			set_css_name ("gridbody");
		}

		construct {
			this.backdrop = new GridBackdrop (28, 48, 1, 4, false, HORIZONTAL);
			this.backdrop.set_parent (this);

			this.overflow = HIDDEN;

			this.notify["beat-count"].connect (() => {
				if (this.grid_orientation == HORIZONTAL) {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_width ();
					}
				} else {
					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_width ();
					}
				}

				this.queue_allocate ();
			});

			this.notify["channel-count"].connect (() => {
				if (this.grid_orientation == HORIZONTAL) {
					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_height ();
					}
				} else {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_height ();
					}
				}

				this.queue_allocate ();
			});

			this.backdrop.notify["cell-width"].connect (() => {
				if (this.grid_orientation == HORIZONTAL) {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_width ();
					}
				} else {
					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_width ();
					}
				}

				this.queue_allocate ();
			});

			this.backdrop.notify["cell-height"].connect (() => {
				if (this.grid_orientation == HORIZONTAL) {
					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_height ();
					}
				} else {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_height ();
					}
				}

				this.queue_allocate ();
			});

			this.backdrop.notify["cells-in-beat"].connect (() => {
				if (this.grid_orientation == HORIZONTAL) {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_width ();
					}
				} else {
					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_width ();
					}
				}

				this.queue_allocate ();
			});

			this.backdrop.notify["beats-in-bar"].connect (() => {
				if (this.grid_orientation == HORIZONTAL) {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_width ();
					}
				} else {
					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_width ();
					}
				}

				this.queue_allocate ();
			});

			this.backdrop.notify["grid-orientation"].connect (() => {
				if (this.grid_orientation == HORIZONTAL) {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_width ();
					}

					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_height ();
					}
				} else {
					if ((Gtk.Adjustment?) this.hadjustment != null) {
						this.hadjustment.upper = this.measure_backdrop_height ();
					}

					if ((Gtk.Adjustment?) this.vadjustment != null) {
						this.vadjustment.upper = this.measure_backdrop_width ();
					}
				}

				this.queue_allocate ();
			});
		}

		public bool get_border (out Gtk.Border border) {
			border = Gtk.Border () {
				left = 0,
				right = 0,
				top = 0,
				bottom = 0
			};

			return true;
		}

		public override void dispose () {
			for (
				var child = this.get_first_child ();
				child != null;
				child = this.get_first_child ()
			) {
				((!) child).unparent ();
			}
		}

		public override void size_allocate (
			int width,
			int height,
			int baseline
		) {
			this.hadjustment.page_size = width;
			this.hadjustment.page_increment = width * 0.9;
			this.hadjustment.step_increment = width * 0.1;

			this.vadjustment.page_size = height;
			this.vadjustment.page_increment = height * 0.9;
			this.vadjustment.step_increment = height * 0.1;

			var beat_width = this.cell_width * this.cells_in_beat;
			var bar_width = beat_width * this.beats_in_bar;

			var horizontal = this.grid_orientation == HORIZONTAL;

			var tile_width = horizontal ? bar_width * 2 : this.cell_height * 12;
			var tile_height = horizontal ? this.cell_height * 12 : bar_width * 2;

			this.backdrop.allocate (
				int.min (width + (int) tile_width, ((int) this.hadjustment.upper) + 1),
				int.min (height + (int) tile_height, ((int) this.vadjustment.upper) + 1),
				baseline,
				new Gsk.Transform ().translate ((!) Graphene.Point () {
					x = (int16) (((int) (-this.hadjustment.value)) % (int) tile_width),
					y = (int16) (((int) (-this.vadjustment.value)) % (int) tile_height)
				})
			);
		}

		private uint measure_backdrop_width () {
			var beat_width = this.cell_width * this.cells_in_beat;
			var bar_width = beat_width * this.beats_in_bar;

			return (bar_width * this.bar_count) - 1;
		}

		private uint measure_backdrop_height () {
			return (this.cell_height * this.channel_count) - 1;
		}
	}
}
