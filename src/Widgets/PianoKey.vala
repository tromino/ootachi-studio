namespace Ootachi.Widgets {
	public class PianoKey : Gtk.Widget {
		public enum KeyType {
			SHARP,
			NATURAL
		}

		private KeyType _key_type;

		public KeyType key_type {
			get {
				return this._key_type;
			}

			construct {
				switch (value) {
					case SHARP:
						this.add_css_class ("sharp");
						break;

					default:
					case NATURAL:
						this.add_css_class ("natural");
						break;
				}

				this._key_type = value;
			}
		}

		class construct {
			set_css_name ("pianokey");
		}

		public PianoKey (KeyType key_type) {
			Object (
				key_type: key_type
			);
		}
	}
}
