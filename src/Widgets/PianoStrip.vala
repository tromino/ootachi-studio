namespace Ootachi.Widgets {
	public class PianoStrip : Gtk.Widget {
		private PianoKey[] key_widgets;

		public Util.GridDescriptor grid {
			get;
			set;

			default = new Util.GridDescriptor (
				20d / 12d,
				14d / 2d,
				12,
				2,
				4,
				4,
				HORIZONTAL,
				true
			);
		}

		public uint octave_count { get; construct; default = 8; }

		class construct {
			set_css_name ("pianostrip");
		}

		construct {
			this.key_widgets = new PianoKey[this.octave_count * 12];

			var sharp_rows = new int[] { 1, 3, 5, 8, 10 };

			for (var key = 0; key < this.key_widgets.length; key++) {
				var key_type = key % 12 in sharp_rows
					? PianoKey.KeyType.SHARP : PianoKey.KeyType.NATURAL;

				this.key_widgets[key] = new PianoKey (key_type);
			}

			foreach (var key_widget in this.key_widgets) {
				if (key_widget.key_type == NATURAL) {
					key_widget.set_parent (this);
				}
			}

			foreach (var key_widget in this.key_widgets) {
				if (key_widget.key_type == SHARP) {
					key_widget.set_parent (this);
				}
			}

			this.grid.grid_changed.connect (() => {
				this.queue_resize ();
				this.queue_allocate ();
			});

			this.notify["grid"].connect (() => {
				this.queue_resize ();
				this.queue_allocate ();

				if ((Util.GridDescriptor?) this.grid != null) {
					this.grid.grid_changed.connect (() => {
						this.queue_resize ();
						this.queue_allocate ();
					});
				}
			});
		}

		public override void dispose () {
			for (
				var child = this.get_first_child ();
				child != null;
				child = this.get_first_child ()
			) {
				((!) child).unparent ();
			}
		}

		public override void measure (
			Gtk.Orientation orientation,
			int for_size,
			out int minimum,
			out int natural,
			out int minimum_baseline,
			out int natural_baseline
		) {
			int size;

			switch (orientation) {
				case HORIZONTAL:
					size = 0;
					break;

				default:
				case VERTICAL:
					var strip_area = this.grid.get_pixel_area (Util.Rect () {
						x = 0,
						y = 0,
						width = 0,
						height = (12 * this.grid.vquants_in_cell) * this.octave_count
					});

					size = (int) strip_area.height;
					break;
			}

			minimum = size;
			natural = size;
			minimum_baseline = -1;
			natural_baseline = -1;
		}

		public override void size_allocate (
			int width,
			int height,
			int baseline
		) {
			for (var key = 0; key < this.key_widgets.length; key++) {
				var grow_up = 0;
				var grow_down = 0;

				var is_natural = this.key_widgets[key].key_type == NATURAL;

				if (is_natural) {
					if (
						key > 1
							&& this.key_widgets[key - 2].key_type == NATURAL
					) {
						grow_up = 1;
					}

					if (
						key < this.key_widgets.length - 2
							&& this.key_widgets[key + 2].key_type == NATURAL
					) {
						grow_down = 1;
					}
				}

				var key_area = this.grid.get_pixel_area (Util.Rect () {
					x = 0,
					y = (key * (int) this.grid.vquants_in_cell) - grow_up,
					width = 0,
					height = this.grid.vquants_in_cell + grow_up + grow_down
				});

				this.key_widgets[key].allocate (
					(int) Math.roundf (width - (is_natural ? 0 : width / 3)),
					((int) key_area.height) + (is_natural ? 0 : 2),
					baseline,
					new Gsk.Transform ().translate ((!) Graphene.Point () {
						x = 0,
						y = key_area.y - (is_natural ? 0 : 1)
					})
				);
			}
		}
	}
}
