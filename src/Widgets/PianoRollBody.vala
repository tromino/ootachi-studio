namespace Ootachi.Widgets {
	public class PianoRollBody : Gtk.Widget, Gtk.Scrollable {
		private GridBackdrop backdrop;
		private Gee.HashMap<uint32, PianoRollPattern> pattern_widgets;

		public Cores.Interfaces.Channel? channel_view { get; set; }

		public Util.GridDescriptor grid {
			get;
			set;

			default = new Util.GridDescriptor (
				20d / 12d,
				14d / 2d,
				12,
				2,
				4,
				4,
				HORIZONTAL,
				true
			);
		}

		public uint bar_count { get; set; default = 64; }
		public uint octave_count { get; set; default = 8; }

		private Gtk.Adjustment _hadjustment;
		private Gtk.Adjustment _vadjustment;

		public Gtk.Adjustment hadjustment {
			get { return this._hadjustment; }

			set construct {
				if ((Gtk.Adjustment?) value != null) {
					value.upper = this.measure_backdrop_width ();

					value.value_changed.connect (() => {
						this.queue_allocate ();
					});

					value.notify["upper"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});

					value.notify["page-size"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});
				}

				this._hadjustment = value;
			}
		}

		public Gtk.Adjustment vadjustment {
			get { return this._vadjustment; }

			set construct {
				if ((Gtk.Adjustment?) value != null) {
					value.upper = this.measure_backdrop_height ();

					value.value_changed.connect (() => {
						this.queue_allocate ();
					});

					value.notify["upper"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});

					value.notify["page-size"].connect (() => {
						value.value = Math.fmax (
							Math.fmin (
								value.value,
								value.upper - value.page_size
							),
							0
						);
					});
				}

				this._vadjustment = value;
			}
		}

		public Gtk.ScrollablePolicy hscroll_policy { get; set; default = NATURAL; }
		public Gtk.ScrollablePolicy vscroll_policy { get; set; default = NATURAL; }

		class construct {
			set_css_name ("pianorollbody");
		}

		construct {
			this.backdrop = new GridBackdrop (this.grid, 0, 0);
			this.backdrop.set_parent (this);

			this.pattern_widgets = new Gee.HashMap<uint32, PianoRollPattern> ();

			var pattern = new Data.Pattern () {
				name = "Cool Pattern",
				relative_end = 48 * 8,
				rows = new Util.WatchableHashMap<uint32, Data.Row> ()
			};

			pattern.rows[12 * 0] = new Data.Row () {
				note = 48,
				instrument = 0,
				effects = {}
			};

			pattern.rows[12 * 1] = new Data.Row () {
				note = 50,
				instrument = 0,
				effects = {}
			};

			pattern.rows[12 * 3] = new Data.Row () {
				note = Data.Row.NOTE_RELEASE,
				instrument = null,
				effects = {}
			};

			pattern.rows[12 * 5] = new Data.Row () {
				note = 51,
				instrument = 0,
				effects = {}
			};

			pattern.rows[12 * 6] = new Data.Row () {
				note = 51,
				instrument = 0,
				effects = {}
			};

			this.channel_view = new Cores.Test.Channels.PulseChannel ("Amazing Channel");
			((!) this.channel_view).patterns[48 * 4] = pattern;

			// Timeout.add (500, () => {
			// 	foreach (var index in pattern.rows.keys.to_array ()) {
			// 		if (pattern.rows[index].note != Data.Row.NOTE_RELEASE) {
			// 			pattern.rows[index] = new Data.Row () {
			// 				note = pattern.rows[index].note + (Random.boolean () ? 1 : -1),
			// 				instrument = pattern.rows[index].instrument,
			// 				effects = pattern.rows[index].effects
			// 			};
			// 		}
			// 	}

			// 	return Source.CONTINUE;
			// });

			this.overflow = HIDDEN;

			this.grid.grid_changed.connect (() => {
				if ((Gtk.Adjustment?) this.hadjustment != null) {
					this.hadjustment.upper = this.measure_backdrop_width ();
				}

				if ((Gtk.Adjustment?) this.vadjustment != null) {
					this.vadjustment.upper = this.measure_backdrop_height ();
				}

				this.queue_allocate ();
			});

			this.notify["grid"].connect (() => {
				this.backdrop.grid = this.grid;

				foreach (var pattern_widget in this.pattern_widgets.values) {
					pattern_widget.grid = this.grid;
				}

				if ((Gtk.Adjustment?) this.hadjustment != null) {
					this.hadjustment.upper = this.measure_backdrop_width ();
				}

				if ((Gtk.Adjustment?) this.vadjustment != null) {
					this.vadjustment.upper = this.measure_backdrop_height ();
				}

				this.queue_allocate ();

				if ((Util.GridDescriptor?) this.grid != null) {
					this.grid.grid_changed.connect (() => {
						if ((Gtk.Adjustment?) this.hadjustment != null) {
							this.hadjustment.upper = this.measure_backdrop_width ();
						}

						if ((Gtk.Adjustment?) this.vadjustment != null) {
							this.vadjustment.upper = this.measure_backdrop_height ();
						}

						this.queue_allocate ();
					});
				}
			});

			this.notify["channel-view"].connect (() => {
				this.sync_patterns ();

				if (this.channel_view != null) {
					((!) this.channel_view).patterns.items_changed.connect (() => {
						this.sync_patterns ();
					});
				}
			});

			this.notify["bar-count"].connect (() => {
				if ((Gtk.Adjustment?) this.hadjustment != null) {
					this.hadjustment.upper = this.measure_backdrop_width ();
				}

				this.queue_allocate ();
			});

			this.notify["octave-count"].connect (() => {
				if ((Gtk.Adjustment?) this.vadjustment != null) {
					this.vadjustment.upper = this.measure_backdrop_height ();
				}

				foreach (var pattern_widget in this.pattern_widgets.values) {
					pattern_widget.octave_count = this.octave_count;
				}

				this.queue_allocate ();
			});

			this.sync_patterns ();
		}

		private void sync_patterns () {
			if (this.channel_view != null) {
				// Create and update widgets for new and reassigned patterns
				foreach (var index in ((!) this.channel_view).patterns.keys) {
					if (!this.pattern_widgets.has_key (index)) {
						this.pattern_widgets[index] = new PianoRollPattern (
							((!) this.channel_view).patterns[index],
							this.grid,
							(int) index
						);

						this.pattern_widgets[index].set_parent (this);
					} else {
						if (this.pattern_widgets[index].pattern != ((!) this.channel_view).patterns[index]) {
							this.pattern_widgets[index].pattern = ((!) this.channel_view).patterns[index];
						}

						if (this.pattern_widgets[index].grid != this.grid) {
							this.pattern_widgets[index].grid = this.grid;
						}
					}
				}

				// Remove widgets for deleted patterns
				// Using to_array () to make an unchanging copy of the keys
				foreach (var index in this.pattern_widgets.keys.to_array ()) {
					if (!((!) this.channel_view).patterns.has_key (index)) {
						this.pattern_widgets[index].unparent ();
						this.pattern_widgets.unset (index);
					}
				}
			} else {
				// Remove all widgets if channel unset
				foreach (var index in this.pattern_widgets.keys.to_array ()) {
					this.pattern_widgets[index].unparent ();
					this.pattern_widgets.unset (index);
				}
			}
		}

		public bool get_border (out Gtk.Border border) {
			border = Gtk.Border () {
				left = 0,
				right = 0,
				top = 0,
				bottom = 0
			};

			return true;
		}

		public override void dispose () {
			for (
				var child = this.get_first_child ();
				child != null;
				child = this.get_first_child ()
			) {
				((!) child).unparent ();
			}
		}

		public override void size_allocate (
			int width,
			int height,
			int baseline
		) {
			this.hadjustment.page_size = width;
			this.hadjustment.page_increment = width * 0.9;
			this.hadjustment.step_increment = width * 0.1;

			this.vadjustment.page_size = height;
			this.vadjustment.page_increment = height * 0.9;
			this.vadjustment.step_increment = height * 0.1;

			var tile_width = (
				(
					this.grid.hquants_in_cell * this.grid.cells_in_beat
				) * this.grid.beats_in_bar
			) * 2;

			var tile_height = this.grid.vquants_in_cell * 12;

			var quant_position = this.grid.get_intersecting (Util.Rect () {
				x = (int) this.hadjustment.value,
				y = (int) this.vadjustment.value,
				width = 0,
				height = 0
			});

			var offset_tiles_x = quant_position.x / (int) tile_width;
			var offset_tiles_y = quant_position.y / (int) tile_height;

			this.backdrop.offset_x = offset_tiles_x * (int) tile_width;
			this.backdrop.offset_y = offset_tiles_y * (int) tile_height;

			var tile_area = this.grid.get_pixel_area (Util.Rect () {
				x = offset_tiles_x * (int) tile_width,
				y = offset_tiles_y * (int) tile_height,
				width = tile_width,
				height = tile_height
			});

			this.backdrop.allocate (
				int.min (width + (int) tile_area.width, ((int) this.hadjustment.upper) + 1),
				int.min (height + (int) tile_area.height, ((int) this.vadjustment.upper) + 1),
				baseline,
				new Gsk.Transform ().translate ((!) Graphene.Point () {
					x = ((int) (-this.hadjustment.value)) + tile_area.x,
					y = ((int) (-this.vadjustment.value)) + tile_area.y
				})
			);

			foreach (var index in this.pattern_widgets.keys) {
				var offset_area = this.grid.get_pixel_area (Util.Rect () {
					x = 0,
					y = 0,
					width = index,
					height = 0
				});

				var offset_pixels_x = ((int) offset_area.width) + (offset_area.width > 0 ? 1 : 0);

				this.pattern_widgets[index].allocate (
					0,
					0,
					baseline,
					new Gsk.Transform ().translate ((!) Graphene.Point () {
						x = ((int) (-this.hadjustment.value)) + offset_pixels_x,
						y = (int) (-this.vadjustment.value)
					})
				);
			}
		}

		private uint measure_backdrop_width () {
			var backdrop_area = this.grid.get_pixel_area (Util.Rect () {
				x = 0,
				y = 0,
				width = ((this.grid.hquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar) * this.bar_count,
				height = 0
			});

			return backdrop_area.width;
		}

		private uint measure_backdrop_height () {
			var backdrop_area = this.grid.get_pixel_area (Util.Rect () {
				x = 0,
				y = 0,
				width = 0,
				height = (this.grid.vquants_in_cell * 12) * this.octave_count
			});

			return backdrop_area.height;
		}
	}
}
