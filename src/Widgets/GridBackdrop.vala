namespace Ootachi.Widgets {
	public class GridBackdrop : Gtk.Widget {
		public Util.GridDescriptor grid {
			get;
			set;

			default = new Util.GridDescriptor (
				20d / 12d,
				14d / 2d,
				12,
				2,
				4,
				4,
				HORIZONTAL,
				true
			);
		}

		// The start position of the grid tile in quants
		public int offset_x { get; set; default = 0; }
		public int offset_y { get; set; default = 0; }

		class construct {
			set_css_name ("gridbackdrop");
		}

		public GridBackdrop (
			Util.GridDescriptor grid,
			int offset_x,
			int offset_y
		) {
			Object (
				grid: grid,
				offset_x: offset_x,
				offset_y: offset_y
			);
		}

		construct {
			this.grid.grid_changed.connect (() => {
				this.queue_draw ();
			});

			this.notify["grid"].connect (() => {
				this.queue_draw ();

				if ((Util.GridDescriptor?) this.grid != null) {
					this.grid.grid_changed.connect (() => {
						this.queue_draw ();
					});
				}
			});

			this.notify["offset-x"].connect (() => {
				this.queue_draw ();
			});

			this.notify["offset-y"].connect (() => {
				this.queue_draw ();
			});
		}

		public override void snapshot (Gtk.Snapshot snapshot) {
			var context = this.get_style_context ();

			Gdk.RGBA bg_1;
			Gdk.RGBA bg_2;
			Gdk.RGBA bg_3;

			Gdk.RGBA grid_1;
			Gdk.RGBA grid_2;
			Gdk.RGBA grid_3;
			Gdk.RGBA grid_4;
			Gdk.RGBA grid_5;

			var failed = false;

			failed = !context.lookup_color ("bg-1", out bg_1) || failed;
			failed = !context.lookup_color ("bg-2", out bg_2) || failed;
			failed = !context.lookup_color ("bg-3", out bg_3) || failed;

			failed = !context.lookup_color ("grid-1", out grid_1) || failed;
			failed = !context.lookup_color ("grid-2", out grid_2) || failed;
			failed = !context.lookup_color ("grid-3", out grid_3) || failed;
			failed = !context.lookup_color ("grid-4", out grid_4) || failed;
			failed = !context.lookup_color ("grid-5", out grid_5) || failed;

			if (failed) {
				warning ("Could not get colors for GridBackdrop");
				return;
			}

			var is_horizontal = this.grid.orientation == HORIZONTAL;

			var self_width = this.get_allocated_width ();
			var self_height = this.get_allocated_height ();

			var sharp_rows = new int[] { 1, 3, 5, 8, 10 };

			var offset_area = this.grid.get_pixel_area (Util.Rect () {
				x = 0,
				y = 0,
				width = this.offset_x,
				height = this.offset_y
			});

			var offset_pixels_x = ((int) offset_area.width) + (offset_area.width > 0 ? 1 : 0);
			var offset_pixels_y = ((int) offset_area.height) + (offset_area.height > 0 ? 1 : 0);

			var visible_quants = this.grid.get_intersecting (Util.Rect () {
				x = offset_pixels_x,
				y = offset_pixels_y,
				width = self_width,
				height = self_height
			});

			var visible_cells = Util.Rect () {
				x = (int) Math.floor (((double) visible_quants.x) / this.grid.hquants_in_cell),
				y = (int) Math.floor (((double) visible_quants.y) / this.grid.vquants_in_cell),
				width = ((int) Math.ceil (((double) (visible_quants.x + visible_quants.width)) / this.grid.hquants_in_cell)) - (int) Math.floor (((double) visible_quants.x) / this.grid.hquants_in_cell),
				height = ((int) Math.ceil (((double) (visible_quants.y + visible_quants.height)) / this.grid.vquants_in_cell)) - (int) Math.floor (((double) visible_quants.y) / this.grid.vquants_in_cell)
			};

			var visible_bars = Util.Rect () {
				x = (int) Math.floor (((double) visible_quants.x) / ((this.grid.hquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar)),
				y = (int) Math.floor (((double) visible_quants.y) / ((this.grid.vquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar)),
				width = ((int) Math.ceil (((double) (visible_quants.x + visible_quants.width)) / ((this.grid.hquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar))) - (int) Math.floor (((double) visible_quants.x) / ((this.grid.hquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar)),
				height = ((int) Math.ceil (((double) (visible_quants.y + visible_quants.height)) / ((this.grid.vquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar))) - (int) Math.floor (((double) visible_quants.y) / ((this.grid.vquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar))
			};

			// The base color for odd bars
			snapshot.append_color (
				bg_2,
				Graphene.Rect () {
					origin = Graphene.Point () {
						x = 0,
						y = 0
					},
					size = Graphene.Size () {
						width = self_width,
						height = self_height
					}
				}
			);

			// Note-axis grid lines and sharp note backgrounds for odd bars
			for (
				var row = is_horizontal ? visible_cells.y : visible_cells.x;
				row < (is_horizontal ? (
					visible_cells.y + visible_cells.height
				) : (
					visible_cells.x + visible_cells.width
				));
				row++
			) {
				var row_area = this.grid.get_pixel_area (Util.Rect () {
					x = row * (int) this.grid.hquants_in_cell,
					y = row * (int) this.grid.vquants_in_cell,
					width = this.grid.hquants_in_cell,
					height = this.grid.vquants_in_cell
				});

				snapshot.append_color (
					grid_1,
					Graphene.Rect () {
						origin = Graphene.Point () {
							x = is_horizontal ? 0 : (row_area.x + (int) row_area.width) - offset_pixels_x,
							y = is_horizontal ? (row_area.y + (int) row_area.height) - offset_pixels_y : 0
						},
						size = Graphene.Size () {
							width = is_horizontal ? self_width : 1,
							height = is_horizontal ? 1 : self_height
						}
					}
				);

				if (this.grid.display_sharps && row % 12 in sharp_rows) {
					snapshot.append_color (
						bg_1,
						Graphene.Rect () {
							origin = Graphene.Point () {
								x = is_horizontal ? 0 : row_area.x - offset_pixels_x,
								y = is_horizontal ? row_area.y - offset_pixels_y : 0
							},
							size = Graphene.Size () {
								width = is_horizontal ? self_width : row_area.width,
								height = is_horizontal ? row_area.height : self_height
							}
						}
					);
				}
			}

			// The same for even bars
			for (
				var bar = (
					is_horizontal ? visible_bars.x : visible_bars.y
				) + (
					((is_horizontal ? visible_bars.x : visible_bars.y) % 2 == 1) ? 0 : 1
				);
				bar < (is_horizontal ? (
					visible_bars.x + visible_bars.width
				) : (
					visible_bars.y + visible_bars.height
				));
				bar += 2
			) {
				var bar_area = this.grid.get_pixel_area (Util.Rect () {
					x = bar * (int) ((this.grid.hquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar),
					y = bar * (int) ((this.grid.vquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar),
					width = ((this.grid.hquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar),
					height = ((this.grid.vquants_in_cell * this.grid.cells_in_beat) * this.grid.beats_in_bar)
				});

				// The base color for even bars
				snapshot.append_color (
					bg_3,
					Graphene.Rect () {
						origin = Graphene.Point () {
							x = is_horizontal ? bar_area.x - offset_pixels_x : 0,
							y = is_horizontal ? 0 : bar_area.y - offset_pixels_y
						},
						size = Graphene.Size () {
							width = is_horizontal ? bar_area.width : self_width,
							height = is_horizontal ? self_height : bar_area.height
						}
					}
				);

				// Note-axis grid lines and sharp note backgrounds for even bars
				for (
					var row = is_horizontal ? visible_cells.y : visible_cells.x;
					row < (is_horizontal ? (
						visible_cells.y + visible_cells.height
					) : (
						visible_cells.x + visible_cells.width
					));
					row++
				) {
					var row_area = this.grid.get_pixel_area (Util.Rect () {
						x = row * (int) this.grid.hquants_in_cell,
						y = row * (int) this.grid.vquants_in_cell,
						width = this.grid.hquants_in_cell,
						height = this.grid.vquants_in_cell
					});

					snapshot.append_color (
						grid_2,
						Graphene.Rect () {
							origin = Graphene.Point () {
								x = is_horizontal ? bar_area.x - offset_pixels_x : (row_area.x + (int) row_area.width) - offset_pixels_x,
								y = is_horizontal ? (row_area.y + (int) row_area.height) - offset_pixels_y : bar_area.y - offset_pixels_y
							},
							size = Graphene.Size () {
								width = is_horizontal ? bar_area.width : 1,
								height = is_horizontal ? 1 : bar_area.height
							}
						}
					);

					if (this.grid.display_sharps && row % 12 in sharp_rows) {
						snapshot.append_color (
							bg_2,
							Graphene.Rect () {
								origin = Graphene.Point () {
									x = is_horizontal ? bar_area.x - offset_pixels_x : row_area.x - offset_pixels_x,
									y = is_horizontal ? row_area.y - offset_pixels_y : bar_area.y - offset_pixels_y
								},
								size = Graphene.Size () {
									width = is_horizontal ? bar_area.width : row_area.width,
									height = is_horizontal ? row_area.height : bar_area.height
								}
							}
						);
					}
				}
			}

			// Time-axis grid lines
			for (
				var col = is_horizontal ? visible_cells.x : visible_cells.y;
				col < (is_horizontal ? (
					visible_cells.x + visible_cells.width
				) : (
					visible_cells.y + visible_cells.height
				));
				col++
			) {
				var line_brightness = 0;

				var cells_in_bar = (this.grid.cells_in_beat * this.grid.beats_in_bar);
				var col_in_beat = col % this.grid.cells_in_beat;
				var col_in_bar = col % cells_in_bar;
				var col_in_tile = col % (cells_in_bar * 2);

				// Highlight lines in even bars
				line_brightness += (col_in_tile > (cells_in_bar - 2)) ? 1 : 0;

				// Highlight beat lines
				line_brightness += col_in_beat == this.grid.cells_in_beat - 1 ? 2 : 0;

				// Highlight bar lines
				line_brightness += col_in_bar == cells_in_bar - 1 ? 1 : 0;

				// Set a color variable based on brightness integer
				Gdk.RGBA line_color;

				switch (line_brightness) {
					case 0:
						line_color = grid_1;
						break;

					case 1:
						line_color = grid_2;
						break;

					case 2:
						line_color = grid_3;
						break;

					case 3:
						line_color = grid_4;
						break;

					default:
					case 4:
						line_color = grid_5;
						break;
				}

				// Draw line with chosen color
				var col_area = this.grid.get_pixel_area (Util.Rect () {
					x = col * (int) this.grid.hquants_in_cell,
					y = col * (int) this.grid.vquants_in_cell,
					width = this.grid.hquants_in_cell,
					height = this.grid.vquants_in_cell
				});

				snapshot.append_color (
					line_color,
					Graphene.Rect () {
						origin = Graphene.Point () {
							x = is_horizontal ? (col_area.x + col_area.width) - offset_pixels_x : 0,
							y = is_horizontal ? 0 : (col_area.y + col_area.height) - offset_pixels_y
						},
						size = Graphene.Size () {
							width = is_horizontal ? 1 : self_width,
							height = is_horizontal ? self_height : 1
						}
					}
				);
			}
		}
	}
}
