namespace Ootachi.Widgets {
	[GtkTemplate (ui = "/org/neocities/tromino/OotachiStudio/MainWindow.ui")]
	public class MainWindow : Gtk.ApplicationWindow {
		[GtkChild]
		private unowned Gtk.Stack main_stack;
		[GtkChild]
		private unowned StartPage start_page;
		[GtkChild]
		private unowned EditorPage editor_page;

		static construct {
			typeof (StartPage).ensure ();
			typeof (EditorPage).ensure ();
		}

		public MainWindow (Application application) {
			Object (
				application: application
			);
		}

		construct {
			var settings = this.get_settings ();
			settings.gtk_hint_font_metrics = true;

			this.start_page.editor_page_shown.connect (() => {
				this.main_stack.visible_child = this.editor_page;
			});
		}
	}
}
