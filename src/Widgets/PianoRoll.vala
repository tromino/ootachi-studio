namespace Ootachi.Widgets {
	[GtkTemplate (ui = "/org/neocities/tromino/OotachiStudio/PianoRoll.ui")]
	public class PianoRoll : MultiView {
		// [GtkChild]
		// private unowned PianoStrip strip;
		// [GtkChild]
		// private unowned PianoRollBody body;

		public Util.GridDescriptor grid { get; set; }

		private Graphene.Point drag_origin = (!) Graphene.Point.zero ();
		private Graphene.Point pointer = (!) Graphene.Point.zero ();
		private Graphene.Point zoom_pivot = (!) Graphene.Point.zero ();

		public int hzoom { get; set; default = 2; }
		public int vzoom { get; set; default = 11; }

		public Util.Animation hzoom_animation { get; set; }
		public Util.Animation vzoom_animation { get; set; }

		private bool animation_callback_active = false;

		static construct {
			typeof (PianoStrip).ensure ();
			typeof (PianoRollBody).ensure ();
		}

		class construct {
			set_css_name ("pianoroll");
		}

		construct {
			this.grid = new Util.GridDescriptor (
				Math.pow (1.4, 2),
				Math.pow (1.2, 11),
				12,
				2,
				4,
				4,
				HORIZONTAL,
				true
			);

			this.hzoom_animation = new Util.Animation (2, TimeSpan.MILLISECOND * 150, SINE);
			this.vzoom_animation = new Util.Animation (11, TimeSpan.MILLISECOND * 150, SINE);

			this.notify["hzoom"].connect (() => {
				this.hzoom_animation.target = (double) this.hzoom;
				this.add_animation_callback ();
			});

			this.notify["vzoom"].connect (() => {
				this.vzoom_animation.target = (double) this.vzoom;
				this.add_animation_callback ();
			});

			var drag_controller = new Gtk.GestureDrag ();
			var pointer_controller = new Gtk.EventControllerMotion ();

			var zoom_controller = new Gtk.EventControllerScroll (
				Gtk.EventControllerScrollFlags.VERTICAL
					| Gtk.EventControllerScrollFlags.DISCRETE
			);

			drag_controller.set_button (Gdk.BUTTON_MIDDLE);

			drag_controller.drag_begin.connect (() => {
				this.drag_origin.x = (float) this.hadjustment.value;
				this.drag_origin.y = (float) this.vadjustment.value;
			});

			drag_controller.drag_update.connect ((offset_x, offset_y) => {
				this.hadjustment.value = this.drag_origin.x - offset_x;
				this.vadjustment.value = this.drag_origin.y - offset_y;
			});

			pointer_controller.motion.connect ((x, y) => {
				if (this.get_direction () != RTL) {
					if (this.start_child != null) {
						this.pointer.x = ((float) x) - ((Gtk.Widget) this.start_child).get_allocated_width ();
					} else {
						this.pointer.x = (float) x;
					}
				} else {
					if (this.end_child != null) {
						this.pointer.x = ((float) x) - ((Gtk.Widget) this.end_child).get_allocated_width ();
					} else {
						this.pointer.x = (float) x;
					}
				}

				if (this.top_child != null) {
					this.pointer.y = ((float) y) - ((Gtk.Widget) this.top_child).get_allocated_width ();
				} else {
					this.pointer.y = (float) y;
				}
			});

			zoom_controller.scroll.connect ((dx, dy) => {
				var modifiers = zoom_controller.get_current_event_state ();
				var shift_held = (modifiers & Gdk.ModifierType.SHIFT_MASK) > 0;

				if (!shift_held) {
					// Horizontal zoom
					var new_hzoom = int.min (
						int.max (
							((int) this.hzoom) - (int) dy,
							-3
						),
						8
					);

					this.zoom_pivot = this.pointer;
					this.hzoom = new_hzoom;
				} else {
					// Vertical zoom
					var new_vzoom = int.min (
						int.max (
							((int) this.vzoom) - (int) dy,
							7
						),
						14
					);

					this.zoom_pivot = this.pointer;
					this.vzoom = new_vzoom;
				}

				return true;
			});

			this.add_controller (drag_controller);
			this.add_controller (pointer_controller);
			this.add_controller (zoom_controller);
		}

		private void add_animation_callback () {
			if (!this.animation_callback_active) {
				this.animation_callback_active = true;

				this.add_tick_callback (() => {
					var hzoom_playing = this.hzoom_animation.playing;
					var vzoom_playing = this.vzoom_animation.playing;
					var playing = hzoom_playing || vzoom_playing;

					var old_hquant_size = this.grid.hquant_size;
					var old_vquant_size = this.grid.vquant_size;
					var old_hadjustment_value = this.hadjustment.value;
					var old_vadjustment_value = this.vadjustment.value;

					this.grid.hquant_size = Math.pow (1.4, this.hzoom_animation.value);
					this.grid.vquant_size = Math.pow (1.2, this.vzoom_animation.value);

					this.hadjustment.value = (
							(old_hadjustment_value + this.zoom_pivot.x)
								* (this.grid.hquant_size / old_hquant_size)
						) - this.zoom_pivot.x;

					this.vadjustment.value = (
							(old_vadjustment_value + this.zoom_pivot.y)
								* (this.grid.vquant_size / old_vquant_size)
						) - this.zoom_pivot.y;

					// Continue calling until animation finished
					this.animation_callback_active = playing;
					return playing;
				});
			}
		}
	}
}
