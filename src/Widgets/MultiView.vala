namespace Ootachi.Widgets {
	public class MultiView : Gtk.Widget, Gtk.Scrollable, Gtk.Buildable {
		protected Gtk.Scrollable? top_child;
		protected Gtk.Scrollable? bottom_child;
		protected Gtk.Scrollable? start_child;
		protected Gtk.Scrollable? end_child;

		protected Gtk.Scrollable _main_child;

		public Gtk.Scrollable main_child {
			get { return this._main_child; }

			set {
				if ((Gtk.Scrollable?) value != null) {
					this.bind_property (
						"hadjustment",
						value,
						"hadjustment",
						SYNC_CREATE | BIDIRECTIONAL
					);

					this.bind_property (
						"vadjustment",
						value,
						"vadjustment",
						SYNC_CREATE | BIDIRECTIONAL
					);

					this.bind_property (
						"hscroll_policy",
						value,
						"hscroll_policy",
						BIDIRECTIONAL
					);

					this.bind_property (
						"vscroll_policy",
						value,
						"vscroll_policy",
						BIDIRECTIONAL
					);
				}

				this._main_child = value;
			}
		}

		private Gtk.Adjustment _hadjustment;
		private Gtk.Adjustment _vadjustment;

		public Gtk.Adjustment hadjustment {
			get { return this._hadjustment; }

			set construct {
				if ((Gtk.Adjustment?) value != null) {
					value.value_changed.connect (() => {
						if (this.top_child != null) {
							((!) this.top_child).hadjustment.value = value.value;
						}

						if (this.bottom_child != null) {
							((!) this.bottom_child).hadjustment.value = value.value;
						}
					});
				}

				this._hadjustment = value;
			}
		}

		public Gtk.Adjustment vadjustment {
			get { return this._vadjustment; }

			set construct {
				if ((Gtk.Adjustment?) value != null) {
					value.value_changed.connect (() => {
						if (this.start_child != null) {
							((!) this.start_child).vadjustment.value = value.value;
						}

						if (this.end_child != null) {
							((!) this.end_child).vadjustment.value = value.value;
						}
					});
				}

				this._vadjustment = value;
			}
		}

		public Gtk.ScrollablePolicy hscroll_policy { get; set; }
		public Gtk.ScrollablePolicy vscroll_policy { get; set; }

		class construct {
			set_css_name ("multiview");
		}

		public bool get_border (out Gtk.Border border) {
			var width = this.get_allocated_width ();
			var height = this.get_allocated_height ();

			var ltr = this.get_direction () == LTR;

			int top_size;
			int bottom_size;
			int start_size;
			int end_size;

			if (this.top_child != null) {
				((Gtk.Widget) this.top_child).measure (
					VERTICAL,
					width,
					null,
					out top_size,
					null,
					null
				);
			} else {
				top_size = 0;
			}

			if (this.bottom_child != null) {
				((Gtk.Widget) this.bottom_child).measure (
					VERTICAL,
					width,
					null,
					out bottom_size,
					null,
					null
				);
			} else {
				bottom_size = 0;
			}

			if (this.start_child != null) {
				((Gtk.Widget) this.start_child).measure (
					HORIZONTAL,
					int.max ((height - top_size) - bottom_size, 0),
					null,
					out start_size,
					null,
					null
				);
			} else {
				start_size = 0;
			}

			if (this.end_child != null) {
				((Gtk.Widget) this.end_child).measure (
					HORIZONTAL,
					int.max ((height - top_size) - bottom_size, 0),
					null,
					out end_size,
					null,
					null
				);
			} else {
				end_size = 0;
			}

			border = Gtk.Border () {
				left = (int16) (ltr ? start_size : end_size),
				right = (int16) (ltr ? end_size : start_size),
				top = (int16) top_size,
				bottom = (int16) bottom_size
			};

			return true;
		}

		public override void dispose () {
			for (
				var child = this.get_first_child ();
				child != null;
				child = this.get_first_child ()
			) {
				((!) child).unparent ();
			}
		}

		public override void size_allocate (
			int width,
			int height,
			int baseline
		) {
			var ltr = this.get_direction () == LTR;

			int top_size;
			int bottom_size;
			int start_size;
			int end_size;

			if (this.top_child != null) {
				((Gtk.Widget) this.top_child).measure (
					VERTICAL,
					width,
					null,
					out top_size,
					null,
					null
				);

				((Gtk.Widget) top_child).allocate (
					width,
					top_size,
					baseline,
					new Gsk.Transform ()
				);
			} else {
				top_size = 0;
			}

			if (this.bottom_child != null) {
				((Gtk.Widget) this.bottom_child).measure (
					VERTICAL,
					width,
					null,
					out bottom_size,
					null,
					null
				);

				((Gtk.Widget) bottom_child).allocate (
					width,
					bottom_size,
					baseline,
					new Gsk.Transform ().translate ((!) Graphene.Point () {
						x = 0,
						y = height - bottom_size
					})
				);
			} else {
				bottom_size = 0;
			}

			if (this.start_child != null) {
				((Gtk.Widget) this.start_child).measure (
					HORIZONTAL,
					int.max ((height - top_size) - bottom_size, 0),
					null,
					out start_size,
					null,
					null
				);

				((Gtk.Widget) start_child).allocate (
					start_size,
					int.max ((height - top_size) - bottom_size, 0),
					baseline,
					new Gsk.Transform ().translate ((!) Graphene.Point () {
						x = ltr ? 0 : width - start_size,
						y = top_size
					})
				);
			} else {
				start_size = 0;
			}

			if (this.end_child != null) {
				((Gtk.Widget) this.end_child).measure (
					HORIZONTAL,
					int.max ((height - top_size) - bottom_size, 0),
					null,
					out end_size,
					null,
					null
				);

				((Gtk.Widget) end_child).allocate (
					end_size,
					int.max ((height - top_size) - bottom_size, 0),
					baseline,
					new Gsk.Transform ().translate ((!) Graphene.Point () {
						x = ltr ? width - end_size : 0,
						y = top_size
					})
				);
			} else {
				end_size = 0;
			}

			((Gtk.Widget) main_child).allocate (
				int.max ((width - start_size) - end_size, 0),
				int.max ((height - top_size) - bottom_size, 0),
				baseline,
				new Gsk.Transform ().translate ((!) Graphene.Point () {
					x = ltr ? start_size : end_size,
					y = top_size
				})
			);
		}

		public void add_child (
			Gtk.Builder builder,
			Object child,
			string? type
		) {
			Gtk.Scrollable scrollable_child;

			if (child is Gtk.Scrollable) {
				scrollable_child = (Gtk.Scrollable) child;
			} else {
				scrollable_child = new Gtk.Viewport (null, null);
				((Gtk.Viewport) scrollable_child).set_child ((Gtk.Widget) child);
			}

			if (type != null) {
				switch ((!) type) {
					case "top":
						if (this.top_child == null) {
							scrollable_child.hadjustment = new Gtk.Adjustment (0, 0, 0, 0, 0, 0);
							scrollable_child.vadjustment = new Gtk.Adjustment (0, 0, 0, 0, 0, 0);

							this.top_child = scrollable_child;
							((Gtk.Widget) scrollable_child).set_parent (this);
						} else {
							warning ("Cannot have more than one child with type \"top\"");
						}

						break;

					case "bottom":
						if (this.bottom_child == null) {
							this.bottom_child = scrollable_child;
							((Gtk.Widget) scrollable_child).set_parent (this);
						} else {
							warning ("Cannot have more than one child with type \"bottom\"");
						}

						break;

					case "start":
						if (this.start_child == null) {
							this.start_child = scrollable_child;
							((Gtk.Widget) scrollable_child).set_parent (this);
						} else {
							warning ("Cannot have more than one child with type \"start\"");
						}

						break;

					case "end":
						if (this.end_child == null) {
							this.end_child = scrollable_child;
							((Gtk.Widget) scrollable_child).set_parent (this);
						} else {
							warning ("Cannot have more than one child with type \"end\"");
						}

						break;
				}
			} else {
				if ((Gtk.Scrollable?) this.main_child == null) {
					this.main_child = scrollable_child;
					((Gtk.Widget) scrollable_child).set_parent (this);
				} else {
					warning ("Cannot have more than one main child");
				}
			}
		}
	}
}
