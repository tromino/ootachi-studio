namespace Ootachi.Widgets {
	[GtkTemplate (ui = "/org/neocities/tromino/OotachiStudio/StartPage.ui")]
	public class StartPage : Gtk.Box {
		public signal void editor_page_shown ();

		[GtkCallback]
		private void show_editor_page () {
			this.editor_page_shown ();
		}
	}
}
