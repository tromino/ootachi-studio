namespace Ootachi.Widgets {
	public class PianoRollPattern : Gtk.Widget {
		public Data.Pattern pattern { get; set; }

		public Util.GridDescriptor grid {
			get;
			set;

			default = new Util.GridDescriptor (
				20d / 12d,
				14d / 2d,
				12,
				2,
				4,
				4,
				HORIZONTAL,
				true
			);
		}

		// The start position of the grid tile in quants
		public int offset_x { get; set; default = 0; }

		public uint octave_count { get; set; default = 8; }

		class construct {
			set_css_name ("pianorollpattern");
		}

		public PianoRollPattern (
			Data.Pattern pattern,
			Util.GridDescriptor grid,
			int offset_x
		) {
			Object (
				pattern: pattern,
				grid: grid,
				offset_x: offset_x
			);
		}

		construct {
			this.grid.grid_changed.connect (() => {
				this.queue_draw ();
			});

			this.notify["grid"].connect (() => {
				this.queue_draw ();

				if ((Util.GridDescriptor?) this.grid != null) {
					this.grid.grid_changed.connect (() => {
						this.queue_draw ();
					});
				}
			});

			this.notify["offset-x"].connect (() => {
				this.queue_draw ();
			});

			this.notify["offset-y"].connect (() => {
				this.queue_draw ();
			});

			this.notify["octave-count"].connect (() => {
				this.queue_draw ();
			});

			this.notify["pattern"].connect (() => {
				if ((Data.Pattern?) this.pattern != null) {
					this.queue_draw ();

					this.pattern.rows.items_changed.connect (() => {
						this.queue_draw ();
					});
				}
			});
		}

		public override void snapshot (Gtk.Snapshot snapshot) {
			var context = this.get_style_context ();

			Gdk.RGBA note_marker_color;
			Gdk.RGBA note_body_color;

			var failed = false;

			failed = !context.lookup_color ("note-marker", out note_marker_color) || failed;
			failed = !context.lookup_color ("note-body", out note_body_color) || failed;

			if (failed) {
				warning ("Could not get colors for PianoRollPattern");
				return;
			}

			var rows = this.pattern.rows;

			var sorted_indexes = new Gee.LinkedList<uint32> ();

			foreach (var index in rows.keys) {
				var insert_at_end = true;

				for (
					var to_index_num = 0;
					to_index_num < sorted_indexes.size;
					to_index_num++
				) {
					var to_index = sorted_indexes[to_index_num];

					if (index < to_index) {
						sorted_indexes.insert (to_index_num, index);
						insert_at_end = false;

						break;
					}
				}

				if (insert_at_end) {
					sorted_indexes.add (index);
				}
			}

			// Now, render the notes
			uint32? last_note_index = null;
			uint8? last_note_value = null;

			foreach (var index in sorted_indexes) {
				var row = rows[index];

				if (row.note != null) {
					var note = (!) row.note;

					// If there's a note to render, then render it
					if (last_note_index != null && last_note_value != null) {
						this.draw_note (
							snapshot,
							(!) last_note_value,
							(!) last_note_index,
							index,
							note_marker_color,
							note_body_color
						);
					}

					if (note == Data.Row.NOTE_RELEASE) {
						last_note_index = null;
						last_note_value = null;
					} else {
						last_note_index = index;
						last_note_value = note;
					}
				}
			}

			// Render the final note if any
			if (last_note_index != null && last_note_value != null) {
				this.draw_note (
					snapshot,
					(!) last_note_value,
					(!) last_note_index,
					this.pattern.relative_end,
					note_marker_color,
					note_body_color
				);
			}
		}

		private void draw_note (
			Gtk.Snapshot snapshot,
			uint8 note,
			uint32 start_index,
			uint32 end_index,
			Gdk.RGBA note_marker_color,
			Gdk.RGBA note_body_color
		) {
			var roundness = 3;
			var marker_width = 4;

			var note_from_top = ((12 * this.octave_count) - 1) - note;

			var note_start = start_index + this.offset_x;

			var note_end = end_index + this.offset_x;

			var note_area = this.grid.get_pixel_area (Util.Rect () {
				x = (int) note_start,
				y = (int) (note_from_top * this.grid.vquants_in_cell),
				width = note_end - note_start,
				height = this.grid.vquants_in_cell
			});

			var offset_area = this.grid.get_pixel_area (Util.Rect () {
				x = 0,
				y = 0,
				width = this.offset_x,
				height = 0
			});

			var offset_pixels_x = ((int) offset_area.width) + (offset_area.width > 0 ? 1 : 0);

			snapshot.push_rounded_clip (Gsk.RoundedRect () {
				bounds = Graphene.Rect () {
					origin = Graphene.Point () {
						x = note_area.x - offset_pixels_x,
						y = note_area.y
					},
					size = Graphene.Size () {
						width = note_area.width,
						height = note_area.height
					}
				},
				corner = {
					Graphene.Size () {
						width = roundness,
						height = roundness
					},
					Graphene.Size () {
						width = roundness,
						height = roundness
					},
					Graphene.Size () {
						width = roundness,
						height = roundness
					},
					Graphene.Size () {
						width = roundness,
						height = roundness
					}
				}
			});

			snapshot.append_color (
				note_marker_color,
				Graphene.Rect () {
					origin = Graphene.Point () {
						x = note_area.x - offset_pixels_x,
						y = note_area.y
					},
					size = Graphene.Size () {
						width = marker_width,
						height = note_area.height
					}
				}
			);

			snapshot.append_color (
				note_body_color,
				Graphene.Rect () {
					origin = Graphene.Point () {
						x = (note_area.x - offset_pixels_x) + marker_width,
						y = note_area.y
					},
					size = Graphene.Size () {
						width = note_area.width - marker_width,
						height = note_area.height
					}
				}
			);

			snapshot.pop ();
		}
	}
}
