namespace Ootachi.Cores.Test.Channels {
	public class PulseChannel : Object, Interfaces.Channel {
		public const uint8 EFFECT_VOLUME = 0;
		public const uint8 EFFECT_ARPEGGIO = 1;
		public const uint8 EFFECT_ARPEGGIO_EXTENSION = 2;
		public const uint8 EFFECT_VOLUME_SLIDE = 3;
		public const uint8 EFFECT_NOTE_SLIDE_UP = 4;
		public const uint8 EFFECT_NOTE_SLIDE_DOWN = 5;
		public const uint8 EFFECT_DETUNE = 6;
		public const uint8 EFFECT_VIBRATO = 7;
		public const uint8 EFFECT_TREMOLO = 8;
		public const uint8 EFFECT_PULSE_WIDTH = 9;

		public string name { get; construct; }
		public uint8 volume { get; set construct; default = 15; }

		public Gee.Map<uint8, Data.EffectDescription> effects { get; construct; }

		public uint8 volume_effect { get; construct; default = EFFECT_VOLUME; }
		public uint8 chord_effect { get; construct; default = EFFECT_ARPEGGIO; }
		public uint8 chord_extension_effect { get; construct; default = EFFECT_ARPEGGIO_EXTENSION; }
		public uint8 note_slide_up_effect { get; construct; default = EFFECT_NOTE_SLIDE_UP; }
		public uint8 note_slide_down_effect { get; construct; default = EFFECT_NOTE_SLIDE_DOWN; }

		public Util.WatchableMap<uint32, Data.Pattern> patterns { get; construct; }

		public PulseChannel (string name) {
			Object (name: name);
		}

		construct {
			this.patterns = new Util.WatchableHashMap<uint32, Data.Pattern> ();
			this.effects = new Gee.HashMap<uint8, Data.EffectDescription> ();

			this.effects[EFFECT_VOLUME] = new Data.EffectDescription () {
				name = "Volume",
				code = null,
				composite = null,
				default_value = 15,
				max_value = 15,
				slide_command = EFFECT_VOLUME_SLIDE,
				piano_visible = true,
				tracker_visible = false
			};

			this.effects[EFFECT_ARPEGGIO] = new Data.EffectDescription () {
				name = "Arpeggio",
				code = '0',
				composite = null,
				default_value = 0,
				max_value = null,
				slide_command = null,
				piano_visible = false,
				tracker_visible = true
			};

			this.effects[EFFECT_ARPEGGIO_EXTENSION] = new Data.EffectDescription () {
				name = "Arpeggio Extension",
				code = '-',
				composite = null,
				default_value = 0,
				max_value = null,
				slide_command = null,
				piano_visible = false,
				tracker_visible = true
			};

			this.effects[EFFECT_VOLUME_SLIDE] = new Data.EffectDescription () {
				name = "Volume Slide",
				code = 'A',
				composite = null,
				default_value = 0,
				max_value = 15,
				slide_command = null,
				piano_visible = false,
				tracker_visible = true
			};

			this.effects[EFFECT_NOTE_SLIDE_UP] = new Data.EffectDescription () {
				name = "Note Slide Up",
				code = 'Q',
				composite = null,
				default_value = 0,
				max_value = null,
				slide_command = null,
				piano_visible = false,
				tracker_visible = true
			};

			this.effects[EFFECT_NOTE_SLIDE_DOWN] = new Data.EffectDescription () {
				name = "Note Slide Down",
				code = 'R',
				composite = null,
				default_value = 0,
				max_value = null,
				slide_command = null,
				piano_visible = false,
				tracker_visible = true
			};

			this.effects[EFFECT_DETUNE] = new Data.EffectDescription () {
				name = "Detune",
				code = 'P',
				composite = null,
				default_value = 128,
				max_value = null,
				slide_command = null,
				piano_visible = true,
				tracker_visible = true
			};

			this.effects[EFFECT_VIBRATO] = new Data.EffectDescription () {
				name = "Vibrato",
				code = '4',
				composite = new Data.CompositeDescription () {
					name_1 = "Vibrato Speed",
					name_2 = "Vibrato Depth",
					max_value_1 = null,
					max_value_2 = null
				},
				default_value = 0,
				max_value = null,
				slide_command = null,
				piano_visible = true,
				tracker_visible = true
			};

			this.effects[EFFECT_TREMOLO] = new Data.EffectDescription () {
				name = "Tremolo",
				code = '7',
				composite = new Data.CompositeDescription () {
					name_1 = "Tremolo Speed",
					name_2 = "Tremolo Depth",
					max_value_1 = null,
					max_value_2 = null
				},
				default_value = 0,
				max_value = null,
				slide_command = null,
				piano_visible = true,
				tracker_visible = true
			};

			this.effects[EFFECT_PULSE_WIDTH] = new Data.EffectDescription () {
				name = "Pulse Width",
				code = 'V',
				composite = null,
				default_value = 0,
				max_value = 15,
				slide_command = null,
				piano_visible = true,
				tracker_visible = true
			};
		}
	}
}
