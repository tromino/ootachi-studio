namespace Ootachi.Cores.Test {
	public class Song : Object, Interfaces.Song {
		public string name { get; set construct; default = "Untitled"; }
		public uint cells_in_beat { get; set construct; default = 4; }
		public uint beats_in_bar { get; set construct; default = 4; }
		public uint total_beats { get; set construct; default = 64; }

		public float tempo {
			get {
				if (this.groove != null) {
					var speed_sum = 0;

					foreach (var speed in (!) this.groove) {
						speed_sum += speed;
					}

					var speed_average = ((float) speed_sum) / ((!) this.groove).size;
					var is_ntsc = true;

					if (is_ntsc) {
						return (3600f / speed_average) / this.cells_in_beat;
					} else {
						return (3000f / speed_average) / this.cells_in_beat;
					}
				} else {
					return 0;
				}
			}

			set construct {
				// Maybe could be useful to calculate a groove approximation?
			}
		}

		public Gee.List<uint16>? groove { get; set; }

		public Channels.PulseChannel channel_pulse_1 { get; construct; }
		public Channels.PulseChannel channel_pulse_2 { get; construct; }
		public Channels.PulseChannel channel_pulse_3 { get; construct; }
		public Channels.NoiseChannel channel_noise { get; construct; }

		construct {
			this.groove = new Gee.ArrayList<uint16> ();

			this.channel_pulse_1 = new Channels.PulseChannel ("Pulse 1");
			this.channel_pulse_2 = new Channels.PulseChannel ("Pulse 2");
			this.channel_pulse_3 = new Channels.PulseChannel ("Pulse 3");
			this.channel_noise = new Channels.NoiseChannel ("Noise");
		}

		public Gee.List<Interfaces.Channel> get_channels () {
			return new Gee.ArrayList<Interfaces.Channel>.wrap ({
				this.channel_pulse_1,
				this.channel_pulse_2,
				this.channel_pulse_3,
				this.channel_noise
			});
		}
	}
}
