namespace Ootachi.Cores.Test {
	public class CoreInfo : Object, Interfaces.CoreInfo {
		public TempoStyle tempo_style { get; construct; default = GROOVE; }

		public Interfaces.Module create_module () {
			return new Module ();
		}
	}
}
