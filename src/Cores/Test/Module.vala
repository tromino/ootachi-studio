namespace Ootachi.Cores.Test {
	public class Module : Object, Interfaces.Module {
		public string name { get; set construct; default = "Untitled"; }
		public string author { get; set construct; default = ""; }
		public string copyright { get; set construct; default = ""; }

		public Gee.List<Interfaces.Song> songs { get; construct; }

		construct {
			this.songs = new Gee.ArrayList<Interfaces.Song> ();
		}
	}
}
