namespace Ootachi.Cores.Interfaces {
	public interface Module : Object {
		public abstract string name { get; set construct; }
		public abstract string author { get; set construct; }
		public abstract string copyright { get; set construct; }

		public abstract Gee.List<Song> songs { get; construct; }
	}
}
