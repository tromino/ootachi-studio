namespace Ootachi.Cores.Interfaces {
	public interface Channel : Object {
		public abstract string name { get; construct; }
		public abstract uint8 volume { get; set construct; }

		public abstract Gee.Map<uint8, Data.EffectDescription> effects { get; construct; }

		public abstract uint8 volume_effect { get; construct; }
		public abstract uint8 chord_effect { get; construct; }
		public abstract uint8 chord_extension_effect { get; construct; }
		public abstract uint8 note_slide_up_effect { get; construct; }
		public abstract uint8 note_slide_down_effect { get; construct; }

		public abstract Util.WatchableMap<uint32, Data.Pattern> patterns { get; construct; }
	}
}
