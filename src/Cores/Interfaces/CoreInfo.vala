namespace Ootachi.Cores.Interfaces {
	public interface CoreInfo : Object {
		public enum TempoStyle {
			BPM,
			GROOVE
		}

		public abstract TempoStyle tempo_style { get; construct; }

		public abstract Module create_module ();
	}
}
