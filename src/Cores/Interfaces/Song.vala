namespace Ootachi.Cores.Interfaces {
	public interface Song : Object {
		public abstract string name { get; set construct; }
		public abstract uint cells_in_beat { get; set construct; }
		public abstract uint beats_in_bar { get; set construct; }
		public abstract uint total_beats { get; set construct; }
		public abstract float tempo { get; set construct; }
		public abstract Gee.List<uint16>? groove { get; set; }

		public abstract Gee.List<Channel> get_channels ();
	}
}
