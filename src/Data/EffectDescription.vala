namespace Ootachi.Data {
	public class EffectDescription {
		public string name;
		public unichar? code;
		public CompositeDescription? composite;
		public uint8 default_value;
		public uint8? max_value;
		public uint8? slide_command;
		public bool piano_visible;
		public bool tracker_visible;
	}
}
