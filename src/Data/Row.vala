namespace Ootachi.Data {
	public class Row {
		public const uint8 NOTE_RELEASE = 255;

		public uint8? note;
		public uint8? instrument;
		public Effect?[] effects;
	}
}
