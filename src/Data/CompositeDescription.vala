namespace Ootachi.Data {
	public class CompositeDescription {
		public string name_1;
		public string name_2;
		public uint8? max_value_1;
		public uint8? max_value_2;
	}
}
