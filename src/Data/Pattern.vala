namespace Ootachi.Data {
	public class Pattern {
		public string name;
		public uint32 relative_end;
		public Util.WatchableMap<uint32, Row> rows;
	}
}
