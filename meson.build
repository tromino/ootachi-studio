project(
	'org.neocities.tromino.OotachiStudio',
	'vala',
	version: '0.1.0',
	license: 'AGPL3+'
)

dependencies = [
	meson.get_compiler('c').find_library('m', required: true),
	dependency('gee-0.8'),
	dependency('gtk4')
]

sources = files(
	'src/Cores/Interfaces/Channel.vala',
	'src/Cores/Interfaces/CoreInfo.vala',
	'src/Cores/Interfaces/Module.vala',
	'src/Cores/Interfaces/Song.vala',
	'src/Cores/Test/Channels/NoiseChannel.vala',
	'src/Cores/Test/Channels/PulseChannel.vala',
	'src/Cores/Test/CoreInfo.vala',
	'src/Cores/Test/Module.vala',
	'src/Cores/Test/Song.vala',
	'src/Data/CompositeDescription.vala',
	'src/Data/Effect.vala',
	'src/Data/EffectDescription.vala',
	'src/Data/Pattern.vala',
	'src/Data/Row.vala',
	'src/Util/Animation.vala',
	'src/Util/GridDescriptor.vala',
	'src/Util/Rect.vala',
	'src/Util/WatchableHashMap.vala',
	'src/Util/WatchableMap.vala',
	'src/Widgets/EditorPage.vala',
	'src/Widgets/GridBackdrop.vala',
	'src/Widgets/MainWindow.vala',
	'src/Widgets/MultiView.vala',
	# 'src/Widgets/OverviewBody.vala',
	'src/Widgets/PianoKey.vala',
	'src/Widgets/PianoRoll.vala',
	'src/Widgets/PianoRollBody.vala',
	'src/Widgets/PianoRollPattern.vala',
	'src/Widgets/PianoStrip.vala',
	'src/Widgets/StartPage.vala',
	'src/Application.vala'
)

stylesheets = files(
	'data/stylesheets/classes/card.css',
	'data/stylesheets/classes/editor-page.css',
	'data/stylesheets/classes/primary-card.css',
	'data/stylesheets/classes/toolbar.css',
	'data/stylesheets/nodes/button.css',
	'data/stylesheets/nodes/gridbody.css',
	'data/stylesheets/nodes/headerbar.css',
	'data/stylesheets/nodes/image.css',
	'data/stylesheets/nodes/label.css',
	'data/stylesheets/nodes/paned.css',
	'data/stylesheets/nodes/pianokey.css',
	'data/stylesheets/nodes/pianostrip.css',
	'data/stylesheets/nodes/scrollbar.css',
	'data/stylesheets/nodes/selection.css',
	'data/stylesheets/nodes/separator.css',
	'data/stylesheets/nodes/tooltip.css',
	'data/stylesheets/nodes/window.css',
	'data/stylesheets/nodes/windowcontrols.css',
	'data/stylesheets/prelude.css'
)

templates = files(
	'data/templates/EditorPage.blp',
	'data/templates/MainWindow.blp',
	'data/templates/PianoRoll.blp',
	'data/templates/StartPage.blp'
)

stylesheet_concat_target = custom_target(
	'stylesheets',
	input: stylesheets,
	output: 'stylesheet.css',
	capture: true,
	command: [
		find_program('cat'),
		'@INPUT@'
	]
)

blueprint_target = custom_target(
	'templates',
	input: templates,
	output: '.',
	command: [
		find_program('blueprint-compiler'),
		'batch-compile',
		'@OUTPUT@',
		'@CURRENT_SOURCE_DIR@',
		'@INPUT@'
	]
)

resources = import('gnome').compile_resources(
	'resources',
	'resources.gresource.xml',
	dependencies: [
		stylesheet_concat_target,
		blueprint_target
	],
	source_dir: [
		# 'data/icons',
		# 'data/stylesheets',
		'_build/data/templates'
	]
)

executable(
	meson.project_name(),
	[
		resources,
		sources
	],
	dependencies: dependencies,
	install: true,
	vala_args: [
		'--enable-experimental-non-null',
		# '--gresourcesdir=../data/icons',
		# '--gresourcesdir=../data/stylesheets',
		'--gresourcesdir=data/templates'
	]
)
